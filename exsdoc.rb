# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://rubydoc.brew.sh/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class Exsdoc < Formula
  desc "The documentation generator written in Expresso and for Expresso"
  homepage "https://www.expresso-lang.org"
  url "https://gitlab.com/exsgo/exsdoc/-/archive/0.3.1/exsdoc-0.3.1.tar.gz"
  sha256 "c184735e619bfbc1ebfd3794d926921d3c5689f21f506530710fac1a80858a01"
  depends_on "mono"
  depends_on "expresso" => :build

  def install
    # ENV.deparallelize  # if your formula fails when building in parallel
    # Remove unrecognized options if warned by configure
    system "make"
    system "make", "install", "PREFIX=#{prefix}"

    (prefix/"bin/exsdoc").write <<~EOS
      #! /bin/sh
      exec mono #{prefix}/lib/exsdoc/exsdoc.exe "$@"
    EOS
  end

  def caveats
    msg = <<~EOF
    exsdoc is now successfully installed. In order to know how to run it,
    see the help message with `exsdoc --help`.
EOF
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test exsdoc`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
